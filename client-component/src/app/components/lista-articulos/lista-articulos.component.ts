import { Component, OnInit } from '@angular/core';

import { ArticuloService } from '../../services/articulo.service';
import { Articulo } from '../../interfaces/articulo.interface';

@Component({
  selector: 'app-lista-articulos',
  templateUrl: './lista-articulos.component.html',
  styleUrls: ['./lista-articulos.component.css']
})
export class ListaArticulosComponent implements OnInit {

  articulosArray: any = [];

  constructor(private servicioArticulo: ArticuloService) { }

  ngOnInit() {
    this.getArticulos();
  }

  getArticulos() {
    this.servicioArticulo.getArticulos().subscribe((data) => {
      this.articulosArray = data;
    });
  }

  deleteArticulo(id: string) {
    this.servicioArticulo.deleteArticulo(id).subscribe(res => { this.getArticulos() });
  }

}
