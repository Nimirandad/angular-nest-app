import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaArticulosComponent } from './components/lista-articulos/lista-articulos.component';


const routes: Routes = [{path: '', component: ListaArticulosComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
