import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Articulo } from '../interfaces/articulo.interface';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticuloService {

  constructor(private http: HttpClient) { }

  getArticulos(): Observable<Articulo[]> {
    return this.http.get<Articulo[]>(`http://localhost:3000/articulos`)
  }

  deleteArticulo(id: string): Observable<Articulo> {
    return this.http.delete<Articulo>(`http://localhost:3000/articulos/delete?id=${id}`)
  }
}
