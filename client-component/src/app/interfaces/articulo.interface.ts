export interface Articulo {
    _id: number;
    created_at: string;
    author: string;
    story_title: string;
    title: string;
    story_url: string;
    url: string;
}