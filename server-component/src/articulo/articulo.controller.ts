import { Controller, Get, Delete, Res, HttpStatus, Query, NotFoundException, Param, Post, Body } from '@nestjs/common';
import { ArticuloService } from './articulo.service';
import { CrearArticuloDTO } from './dto/articulo.dto';

@Controller('articulos')
export class ArticuloController {

    constructor(private servicioArticulo: ArticuloService) { }

    @Get('/setdatabase')
    async consumirData() {
        const insert = await this.servicioArticulo.consumirData();

        if (!insert) throw new NotFoundException('No se realizo la insercion');
        
        return insert;
    }

    @Get('/')
    async getArticulos(@Res() res) {
        const articulos = await this.servicioArticulo.getArticulos();

        if (articulos.length === 0) throw new NotFoundException('No existen articulos');

        return res.status(HttpStatus.OK).json({
            articulos
        });
    }

    @Get('/:id')
    async getArticulo(@Res() res, @Param('id') id) {
        const articulo = await this.servicioArticulo.getArticulo(id);

        if (!articulo) throw new NotFoundException('No existe el articulo');

        return res.status(HttpStatus.OK).json(articulo);
    }

    @Post('/create')
    async createArticulo(@Res() res, @Body() crearArticulo: CrearArticuloDTO) {
        const articulo = await this.servicioArticulo.createArticulo(crearArticulo)
        return res.status(HttpStatus.OK).json({
            mensaje: 'Articulo creado exitosamente',
            articulo
        })
    }

    @Delete('/delete')
    async deleteArticulo(@Res() res, @Query('id') id) {
        const articulo = await this.servicioArticulo.deleteArticulo(id);

        if (!articulo) throw new NotFoundException('El articulo no existe');

        return res.status(HttpStatus.OK).json({
            mensaje: 'Articulo eliminado satisfactoriamente',
            articulo
        });
    }
}
