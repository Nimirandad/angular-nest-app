import { HttpModule, Module } from '@nestjs/common';
import { ArticuloController } from './articulo.controller';
import { ArticuloService } from './articulo.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticuloSchema } from './schemas/articulo.schema';

@Module({
  imports: [HttpModule, MongooseModule.forFeature([{name: 'Articulo', schema: ArticuloSchema}])],
  controllers: [ArticuloController],
  providers: [ArticuloService]
})
export class ArticuloModule { }
