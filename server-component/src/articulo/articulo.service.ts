import { Injectable, HttpService,  OnModuleInit } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose'

import { Articulo } from './interfaces/articulo.interface';
import { CrearArticuloDTO } from './dto/articulo.dto';

@Injectable()
export class ArticuloService {

    constructor(@InjectModel('Articulo') private articuloModel: Model<Articulo>, private http: HttpService) { }

    async getArticulos(): Promise<Articulo[]> {
        const articulos = await this.articuloModel.find();
        return articulos;
    }

    async getArticulo(id: string): Promise<Articulo> {
        const articulo = await this.articuloModel.findById(id);
        return articulo;
    }

    async createArticulo(crearArticulo: CrearArticuloDTO): Promise<Articulo> {
        const articulo = new this.articuloModel(crearArticulo);
        return await articulo.save();
    }

    async deleteArticulo(id: string): Promise<Articulo> {
        const articuloEliminado = await this.articuloModel.findByIdAndDelete(id);
        return articuloEliminado;
    }


    // Consume API to populate the database
    consumirData() {
        const datos = this.http.get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs').pipe(
            map(res => this.recepcionDatos(res.data.hits))
        );
        return datos
    }

    async recepcionDatos(res: any) {
        const articulos = await res;
        const array = Object.keys(articulos).length;

        for (let i = 0; i < array; i++) {
            const id = articulos[i].objectID;
            const fechaCreacion = articulos[i].created_at;
            const autor = articulos[i].author;
            const tituloHistoria = articulos[i].story_title;
            const titulo = articulos[i].title;
            const urlHistoria = articulos[i].story_url;
            const url = articulos[i].url;

            this.asignarValor(id, fechaCreacion, autor, tituloHistoria, titulo, urlHistoria, url);
        }
    }

    asignarValor(id: number, fecha: string, autor: string, tituloH: string, titulo: string, urlH: string, url: string) {
        const articulo = new this.articuloModel;

        articulo._id = id;
        articulo.created_at = fecha;
        articulo.author = autor;
        articulo.story_title = tituloH;
        articulo.title = titulo;
        articulo.story_url = urlH;
        articulo.url = url;

        articulo.save()
    }
}
