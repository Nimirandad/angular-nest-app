import { Schema } from 'mongoose';

export const ArticuloSchema = new Schema({
    _id: { type: Number, unique: true },
    created_at: String,
    author: String,
    story_title: String,
    title: String,
    story_url: String,
    url: String
}, { versionKey: false, _id: false });