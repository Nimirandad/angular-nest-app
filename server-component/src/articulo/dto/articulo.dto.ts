export class CrearArticuloDTO {
    readonly _id: number;
    readonly created_at: string;
    readonly author: string;
    readonly story_title: string;
    readonly title: string;
    readonly story_url: string;
    readonly url: string;
}