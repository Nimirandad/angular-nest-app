import { Document } from 'mongoose';

export interface Articulo extends Document {
    _id: Number;
    created_at: String;
    author: String;
    story_title: String;
    title: String;
    story_url: String;
    url: String;
}