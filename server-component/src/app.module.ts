import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArticuloModule } from './articulo/articulo.module';

import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [ArticuloModule, MongooseModule.forRoot('mongodb+srv://user:12345@clusterprueba.vwcs9.mongodb.net/test?retryWrites=true&w=majority')],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
