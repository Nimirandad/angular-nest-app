# angular-nest-app

## Installation

For the server component and the client component it is necessary to install the dependencies with the following command:

```bash
npm install
```

## Running the app

```bash
# For server component
npm run start:dev

# For client component
ng serve
```

## Populate the database
This project works with mongodb, so it has a cluster assigned to fill the database. It should be mentioned that the project does not fill the database automatically and does not connect to the api to update the data. So it is necessary to go to the following path (in browser or postman / insomnia) to perform the filling:

```bash
http://localhost:3000/articulos/setdatabase
```

## REST Paths
The project uses only 2 routes to consume the REST service, these are:

```bash
# To get all articles
http://localhost:3000/articulos

# To delete an article
http://localhost:3000/articulos/delete?id=${id}
```

## Missing Features
Due to knowledge issues, the following requested characteristics were not covered:

- Pull the API's data automatically to the database to populate it.
- Connect to the API every 1 hour, to refresh the data in the database.
- Format the date as it is shown in the wireframe.
- Sort the data according to date (by default they are sorted from the most recent date, but when making a new insertion it gets messy).
- The same wireframe view.
- Unit testing and test coverage on server component. (Also Gitlab pipeline).
- Both components aren't dockerized.